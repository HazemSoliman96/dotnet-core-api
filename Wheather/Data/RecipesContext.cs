﻿using Microsoft.EntityFrameworkCore;
using Wheather.Models;

namespace Wheather.Data
{
    public class RecipesContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Recipe> Recipes { get; set; }

        public RecipesContext(DbContextOptions options) : base(options){}
    }
}
